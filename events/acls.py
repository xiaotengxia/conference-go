from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json
from random import randint

url = "https://api.pexels.com/v1/search/"


def get_photo(city, state):

    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": f"{city} {state}"}
    res = requests.get(url, params=params, headers=headers)
    result = json.loads(res.text)
    index = randint(0, len(result["photos"]))
    return result["photos"][index]["url"]


def get_lat_lon(city, state):
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    geo_params = {"appid": OPEN_WEATHER_API_KEY, "q": f"{city}, {state}, USA"}
    res = requests.get(geo_url, params=geo_params)
    the_json = res.json()
    lat = the_json[0]["lat"]
    lon = the_json[0]["lon"]
    return lat, lon


def get_weather_data(city, state):
    lat, lon = get_lat_lon(city, state)
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    weather_params = {
        "appid": OPEN_WEATHER_API_KEY,
        "lat": lat,
        "lon": lon,
        "units": "imperial",
    }

    res = requests.get(weather_url, params=weather_params)
    the_json = res.json()
    return {
        "temp": the_json["main"]["temp"],
        "description": the_json["weather"][0]["description"],
    }
